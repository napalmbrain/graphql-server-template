import { ApolloLink, DocumentNode } from '@apollo/client/core';
import { ApolloClient, InMemoryCache } from '@apollo/client/core';

interface QueryArguments {
  query: DocumentNode;
}

interface SubscriptionArguments extends QueryArguments {
  trigger?: () => Promise<void>;
}

export class Client {
  client: ApolloClient<any>;

  constructor(link: ApolloLink) {
    this.client = new ApolloClient({
      link,
      cache: new InMemoryCache()
    });
  }

  query(args: QueryArguments) {
    return this.client.query({
      query: args.query,
      fetchPolicy: 'no-cache'
    });
  }

  mutate(args: QueryArguments) {
    return this.client.mutate({
      mutation: args.query,
      fetchPolicy: 'no-cache'
    });
  }

  async subscribe(args: SubscriptionArguments) {
    const observer = this.client.subscribe({
      query: args.query,
      fetchPolicy: 'no-cache'
    });
    let subscription: any = null;
    const promise = new Promise((resolve, reject) => {
      subscription = observer.subscribe({
        next: (data) => resolve(data),
        error: (error) => reject(error)
      });
    }).finally(() => {
      subscription.unsubscribe();
    });
    await new Promise((resolve) => {
      setTimeout(resolve, 100);
    });
    if (args.trigger) {
      await args.trigger();
    }
    return promise;
  }
}
