import { pubsub } from '../../lib/constants';

export default {
  Mutation: {
    publish(root: any, args: any, context: any) {
      pubsub.publish('pubsub', { subscribe: 'value' });
      return 'value';
    }
  }
};
